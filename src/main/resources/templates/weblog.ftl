<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><#if serverid??>${serverid!''}</#if>服务器实时日志</title>
    <script src="//cdn.bootcss.com/jquery/2.1.4/jquery.js"></script>
    <style type="text/css">
        body{margin:0;padding:0}
    </style>
</head>
<body>
<div id="log-container" style="height: 900px; overflow-y: scroll; background: #292929; color: #ececec; padding: 20px;">
    <div>
    </div>
</div>
</body>
<script>
    $(document).ready(function() {

//        var height = window.screen.height;
//        $("#log-container").css("height", (height-100)+"px");
        // 指定websocket路径
        var websocket = new WebSocket('ws://weblog.leshixiyun.com/web/log?serverid=${serverid}');
        websocket.onmessage = function(event) {
            // 接收服务端的实时日志并添加到HTML页面中
            $("#log-container div").append(event.data);
            console.log(event.data)
            // 滚动条滚动到最低部
            $("#log-container").scrollTop($("#log-container div").height() - $("#log-container").height());
        };
    });
</script>
</body>
</html>