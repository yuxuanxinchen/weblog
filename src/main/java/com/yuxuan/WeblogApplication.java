package com.yuxuan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.socket.config.annotation.EnableWebSocket;

import javax.servlet.http.HttpSession;

@ServletComponentScan
@Controller
@EnableWebSocket
@SpringBootApplication
public class WeblogApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeblogApplication.class, args);
	}

	@RequestMapping(value = "/")
	public String home(){
		return "redirect:/index";
	}

	@RequestMapping(value = "/index")
	public String index(){
		return "index";
	}

	@RequestMapping(value = "/viewlog")
	public String  viewServerLog(String serverid, ModelMap mMap){
		if(serverid != "9091" && serverid != "9092" && !StringUtils.isEmpty(serverid)){
			mMap.put("serverid", serverid);
			return "weblog";
		}
		if(StringUtils.isEmpty(serverid)){
			return "redirect:/index";
		}
		return "error";
	}

	@RequestMapping(value = "login")
	public String login(String username, String password, HttpSession session){
		if(StringUtils.isEmpty(username) || StringUtils.isEmpty(password)){
			return "login";
		}
		if(username.equals("weblog") && password.equals("weblog")){
			session.setAttribute("CURR_USER",username);
			return "redirect:/index";
		}
		return "login";
	}

}
